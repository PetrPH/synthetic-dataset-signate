# Synthetic dataset Signate
The repository is official implementation of runner-up solution in Signate ['Modules for Generating Pre-training Image Datasets Contest: The Challenge of Training AI without Natural Images' competition](https://signate.jp/competitions/1071).

_**It is great for generating huge dataset of clean labeled data.**_

## How to use it?
1. Adjust image resolution, number of classes, and number of generated images per class in `sources/params.json`.
2. Run
```python
from sources.generator import Generator
gen = Generator()
gen.get_params('sources/params.json')
gen.generate('./out')
```

3. Enjoy your generated images and pretrain your neural network.

## Example of generated images
![Visualization of generated images](materials/example_of_generated.png)

## Novelty and principle
The combination of rendered numbers corresponding to class_id and a visual pattern is unique. The novel idea is that there is a single template (Bezier curve) that includes information about class, but it is covered by another instances of the same template that are mutually rotated, resized, shifted, and vary in color. This means, the network is forced to learn universal kernels useful for huge variety of tasks. The partially visible class_id leverage fast convergence of training. The complexity of the produced image prevent model overfitting on the training dataset. For more details, see the [pdf material](materials/report_en.pdf).

## Benefits
**Fast convergence:**
The class_id within the image is easy to solve and helps the network to go fast from randomly initialized kernels to at least some form of kernels able to capture visual information.

**Complexity of the task:**
Class_id is not always visible and covered by a visual pattern in most cases. The visual pattern is class dependent, but it is hard to learn all the possibilities, i. e., low probability that the model will overfit the train data and achieve maximum performance. To solve the complicated task, it is also forced to establish general kernels.

**Scalability**:
The algorithm is not limited to a fixed number of produced classes, it can be extended easily to produce 'infinity' number of distinct classes

**Computation complexity:**
Generation of Bezier curves is fast and there are several ways of how to control speed-precision tradeoff.
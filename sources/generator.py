import os
import json
import numpy as np
import cv2 as cv
import random
import bezier
from math import cos, sin

# set seeds to preserve reproducibility of the results
random.seed(9)
np.random.seed(9)

class Generator():
    @classmethod
    def get_params(cls, params_path: str):
        """Read the parametes into class property params

        Args:
            params_path (str): absolute path where the file 'params.json' is stored

        Returns:
            True if successfull, False otherwise
        """        
        cls.debug = True
        try:
            with open(os.path.join(params_path, 'params.json')) as f:
                cls.params = json.load(f)
        except:
            print("cannot read the parameters")
            return False
        return True


    @classmethod
    def generate(cls, out_path: str):
        """ The main method that creates cls.params['num_classes'] folders within the out_path.
        Each folder contains cls.params['num_images'] unique images. Images within one folder
        belong to the same class.

        Args:
            out_path (str): absolute path where the generated images are stored
        """        
        if cls.debug:
            print(out_path)

        for c in range(0,  cls.params['num_classes']):
            os.makedirs(os.path.join(out_path, str(c)), exist_ok=True)
            for i in range(cls.params['num_images']):
                img = cls.__generate_image(c, cls.params['img_size'])
                cv.imwrite(os.path.join(out_path, str(c), '{:03d}.png'.format(i)), img)


    @classmethod
    def __generate_image(cls, id_class: int, res: int):
        """ Create one unique image for a particular class. It creates image with
        black background, some numbers and bezier curves.

        Args:
            id_class (int): 0..n, where n=1000 for the competition
            res (int): resolution of the generated images, 256 for the competition

        Returns:
            np.uint8 array BGR generated image
        """       

        #here set the parameters that uniquely determine the generated class 
        degree   = (id_class % 11)*3 + 3
        smooth   = id_class % 9
        thick    = id_class % 7 + 1
        density  = id_class % 15 + 1
        start    = (0.5 - (id_class // 200) / 10) / 2
        decrease = 0.995 + random.uniform(-0.005, 0.003)

        res2 = res*2 #the image is rendered in bigger res and downscaled later
        ppc  = res//8 #points per curve
        t    = np.linspace(0, 1, ppc)
        rx   = 0.5 + random.uniform(-0.1, 0.1)
        ry   = 0.5 + random.uniform(-0.1, 0.1)

        # the colors of generated bezier curves
        col1 = np.asarray([random.randint(0,255), random.randint(0,255), random.randint(0,255)])
        col2 = np.asarray([random.randint(0,255), random.randint(0,255), random.randint(0,255)])
        col3 = np.asarray([random.randint(0,255), random.randint(0,255), random.randint(0,255)])
        img  = np.full((res2,res2,3), (0), dtype='uint8')

        # draw randomly positioned and sized numbers equal to the id_class. That helps the network 
        # to converge faster. To avoid too big simplicity, the numbers overlap each others and are
        # moreover covered by bezier curves
        font = cv.FONT_HERSHEY_SIMPLEX
        for _ in range(0, random.randint(1,30)):
            col = random.randint(0,255)
            font_col = [col,col,col]
            org = (res+random.randint(-250,250), res+random.randint(-250,250))
            img = cv.putText(img, str(id_class), org, font, random.uniform(0.1,6), font_col, random.randint(1,15), cv.LINE_AA)

        # definition of control points of the bezier curve. We have one template of bezier curve,
        # that is then rotated around center a draw in descending size.
        x_control     = np.linspace(-start, 0.4, degree)
        y_control     = np.random.uniform(0, 1.0, degree)
        y_control[0]  = ry
        y_control[-1] = ry
        x_control    -= rx
        y_control    -= ry

        for i in range(630, 0, -density):
            # the bezier curve is smaller and smaller
            x_control *= decrease
            y_control *= decrease

            # bezier curves are draw rotated around center.
            theta = np.deg2rad(i)
            x_control_rot = x_control*cos(theta) - y_control*sin(theta) + rx
            y_control_rot = y_control*cos(theta) + x_control*sin(theta) + ry

            # compute points on bezier curve from the control points
            nodes  = np.asfortranarray([x_control_rot, y_control_rot])
            curve  = bezier.Curve(nodes, degree=degree-1)
            points = curve.evaluate_multi(np.asfortranarray(t))
            draw_points = (np.asarray([points[0]*res2, points[1]*res2]).T).astype(np.int32) 

            # swap of colors. It is may happened that the colors are not swapped because
            # 630/density == 315 does not hold for arbitrary density. That also creates the 
            # uniqueness of the classes.
            if i == 315:
                col1 = col2
                col2 = col3

            # compute the final color of the particular bezier curve and draw it.
            if i>=315: w = (i-315)/630
            else: w   = i/315
            col = ((1.0-w)*col1 + w*col2).astype('uint8')
            cv.polylines(img, [draw_points], False, col.tolist(), thickness=thick,lineType=cv.LINE_AA) 

        # apply smoothing that makes also the uniqueness of the class, downscale the image into the required
        # resolution and return it. That's all:)
        if smooth: img = cv.blur(img,(smooth*2+1,smooth*2+1))
        img = cv.resize(img, (res,res), interpolation=cv.INTER_LINEAR)   
        return img